// Quels champs ai-je besoin de consulter ? (lecture)
let inputs;

// Quels champs ai-je besoin de mettre à jour ? (ecriture)
let infos;
let indicateur_faible;
let indicateur_moyen;
let indicateur_fort;

onload = init;

// La fonction lancée une fois que la page est chargée
function init() {
    // Récupération de tous les input de inscription.html dans le tableau "inputs"
    inputs = document.getElementsByTagName("input");

    // Récupération de tous les textes qui passeront en rouge si erreur
    infos = document.getElementsByTagName("p");

    indicateur_faible = document.getElementById("faible");
    indicateur_moyen = document.getElementById("moyen");
    indicateur_fort = document.getElementById("fort");

    inputs.user.addEventListener("input", verifierUtilisateur);
    inputs.mail.addEventListener("input", verifierMail);
    inputs.mdp.addEventListener("input", verifierMdp);

    let formulaire = document.getElementById("inscription-form");
    formulaire.addEventListener("submit", inscrire);
}

function inscrire(event) {
    // Je fais sauter le comportement par défaut de l'evenement
    // "soumission de formulaire"
    event.preventDefault();

    // Je récupère les valeurs saisies par l'utilisateur
    let nom = inputs.user.value;
    let mail = inputs.mail.value;
    let mdp = inputs.mdp.value;

    // Je crée une instance d'utilisateur qui regroupe ces informations
    let utilisateur = {
        "name": nom,
        "mail": mail,
        "mdp": mdp
    };

    // Création du tableau que je vais enregistrer dans le localstorage
    let utilisateurs_array = [];

    // Je récupère les utilisateurs existants si jamais ils existent
    let utilisateurs_existants = localStorage.getItem("users");
    if (utilisateurs_existants) {
        utilisateurs_array = JSON.parse(utilisateurs_existants);
    }

    // J'ajoute l'utilisateur créé précédemment à mon tableau
    utilisateurs_array.push(utilisateur);

    // Je souhaite ecrire le contenu de "utilisateur" dans le localStorage
    localStorage.setItem("users", JSON.stringify(utilisateurs_array));
}

function verifierUtilisateur() {
    // Dans l'input "user", j'accède à ce qui a été tapé par l'utilisateur
    if (inputs.user.value.length < 3) {
        infos.userInfo.style.color = "red";
    } else {
        infos.userInfo.style.color = "black";
    }
}

function verifierMail() {
    if (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,}$/.test(inputs.mail.value)) {
        infos.mailInfo.style.color = "black";
    } else {
        infos.mailInfo.style.color = "red";
    }
}

function verifierMdp() {
    // Vérification si le mot de passe est valide ou pas
    if (/^((?=.*?[a-zA-Z])(?=.*?[0-9])(?=.*[#?!@$%^&*-])).{6,}$/.test(inputs.mdp.value)) {
        infos.passInfo.style.color = "black";
    } else {
        infos.passInfo.style.color = "red";
    }

    // Vérification du niveau de sécurité du mot de passe
    let symboleOK = /[#?!@$%^&*-]/.test(inputs.mdp.value);
    let nombreOK = /[\d]/.test(inputs.mdp.value);
    
    // Vérification des contraintes de mdp fort
    if (inputs.mdp.value.length > 9
        && symboleOK
        && nombreOK) {
            indicateur_faible.style.opacity = "100%";
            indicateur_moyen.style.opacity = "100%";
            indicateur_fort.style.opacity = "100%";
    }
    // Vérification des contraintes de mdp moyen
    else if (inputs.mdp.value.length > 6
        && (symboleOK || nombreOK)) {
            indicateur_faible.style.opacity = "100%";
            indicateur_moyen.style.opacity = "100%";
            indicateur_fort.style.opacity = "0%";
    } else {
            indicateur_faible.style.opacity = "100%";
            indicateur_moyen.style.opacity = "0%";
            indicateur_fort.style.opacity = "0%";

            if (inputs.mdp.value.length == 0) {
                indicateur_faible.style.opacity = "0%";
            }
    }
}