const WIDTH = 3;
const HEIGHT = 2;

let images = [
    "../img/memory-legume/1.svg",
    "../img/memory-legume/1.svg",
    "../img/memory-legume/2.svg",
    "../img/memory-legume/2.svg",
    "../img/memory-legume/3.svg",
    "../img/memory-legume/3.svg",
    "../img/memory-legume/4.svg",
    "../img/memory-legume/4.svg",
    "../img/memory-legume/5.svg",
    "../img/memory-legume/5.svg",
    "../img/memory-legume/6.svg",
    "../img/memory-legume/6.svg"
];

let pileDeCartes = [];
let verifEnCours = false;
let cartesRetournees = 0;
let victoire;
let plateau;

// Au chargement de la page, je déclenche la fonction toto
onload = init;

function init() {
    victoire = document.getElementById("victory-prompt");
    plateau = document.getElementById("plateau");

    // Permet de conserver pile le bon nombre d'images compte tenu de la taille de la grille
    images = images.slice(0, WIDTH * HEIGHT);
    shuffleArray(images);

    genererGrille();

    document.addEventListener("keypress", e => {
        if (e.key === " ") {
            shuffleArray(images);
            genererGrille();
        }
    });
}

function genererGrille() {
    // Je vide le contenu du plateau
    plateau.innerHTML = "";
    victoire.style.opacity = "0%";
    cpt = 0;

    if (WIDTH * HEIGHT == images.length && images.length % 2 == 0) {
        // Je génère 3 lignes dans mon tableau
        for (let i = 0; i < HEIGHT; i++) {
            // Création d'une balise tr (table row)
            let tr_elem = document.createElement("tr");

            // J'ajoute du contenu à mon tr
            for (let j = 0; j < WIDTH; j++) {
                // Création d'une balise td (table data)
                let td_elem = document.createElement("td");

                // Création d'une balise image (qui correspond à une carte face cachée)
                let img_elem = document.createElement("img");
                img_elem.src = "../img/question.svg";
                img_elem.alt = "Une carte face cachée";
                img_elem.classList.add("memory-image", "playable");

                img_elem.id = i * WIDTH + j;

                // Je déclenche une action quand je clique sur une image
                img_elem.addEventListener("click", retournerCarte);

                // Chaque image créée doit être affectée au td correspondant
                td_elem.appendChild(img_elem);
                
                tr_elem.appendChild(td_elem);
                /* resultat de nos appendChild successifs
                <tr>
                    <td>
                        <img src="../img/question.svg"></img>
                    </td>
                </tr>
                */
            }


            // J'ajoute le tr créé précédemment à mon plateau de jeu
            plateau.appendChild(tr_elem);
        }
    } else {
        let row = document.createElement("tr");
        let cell = document.createElement("td");
        let img = document.createElement("img");

        img.src = "../img/fucking-up.png";
        cell.appendChild(img);
        row.appendChild(cell);
        plateau.appendChild(row);
    }
}

// event correspondant a l'action qui a déclenché la fonction (clic sur une carte)
function retournerCarte(event) {
    // Si on a déjà deux cartes retournées, on ne retourne pas la dernière carte cliquée
    if (pileDeCartes.length == 2) {
        return;
    }

    // console.log(event);

    // Je vais récupérer l'identifiant de l'image sur laquelle je viens de cliquer
    let index = event.srcElement.id;

    // Je déclenche l'animation de rotation, et je désactive le clic sur cette carte
    event.srcElement.classList.add("rotated");
    event.srcElement.removeEventListener("click", retournerCarte);
    event.srcElement.classList.remove("playable");

    // Je garde en mémoire la carte sur laquelle le joueur a cliqué
    pileDeCartes.push(event.srcElement);

    setTimeout(() => {
        event.srcElement.classList.remove("rotated");
        event.srcElement.src = images[index];

        comparerCartes();
    }, 300);
}

function comparerCartes() {
    if (pileDeCartes.length != 2 || verifEnCours)
        return;

    verifEnCours = true;
    
    if (images[pileDeCartes[0].id] == images[pileDeCartes[1].id]) {
        setTimeout(() => {
            pileDeCartes[0].style.background = "rgb(202,255,168)";
            pileDeCartes[1].style.background = "rgb(202,255,168)";

            pileDeCartes = [];
            verifEnCours = false;
            cpt += 2;

            verifierVictoire();
        }, 600);
    } else {
        setTimeout(() => {
            pileDeCartes[0].classList.add("rotated");
            pileDeCartes[1].classList.add("rotated");

            setTimeout(() => {
                pileDeCartes[0].classList.remove("rotated");
                pileDeCartes[1].classList.remove("rotated");
                pileDeCartes[0].src = "../img/question.svg";
                pileDeCartes[1].src = "../img/question.svg";
                pileDeCartes[0].addEventListener("click", retournerCarte);
                pileDeCartes[1].addEventListener("click", retournerCarte);
                pileDeCartes[0].classList.add("playable");
                pileDeCartes[1].classList.add("playable");

                pileDeCartes = [];
                verifEnCours = false;
            }, 300);
        }, 2000);    
    }
}

function verifierVictoire() {
    if (cpt == WIDTH * HEIGHT) {
        victoire.style.opacity = "100%";
    }
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}