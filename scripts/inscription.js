
onload = init;

let inputs;
let infos;

function init() {
    inputs = document.getElementsByTagName("input");
    infos = document.getElementsByTagName("p");
    let btn_sinscrire = document.getElementById("sinscrire");

    btn_sinscrire.addEventListener("click", checkForm);
}

function checkForm() {
    verifierUser();
    verifierMail();
    verifierPass();
}

function verifierUser() {
    if (inputs.user.value.length < 3) {
        infos.userInfo.style.color = "red";
    } else {
        infos.userInfo.style.color = "black";
    }
}

function verifierMail() {
    let mailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,}$/g;
    let filter = inputs.mail.value.match(mailRegex);
    if (!(filter != null && filter.length == 1 && filter[0] == inputs.mail.value)) {
        infos.mailInfo.style.color = "red";
    } else {
        infos.mailInfo.style.color = "black";
    }
}

function verifierPass() {
    let chiffreOK = /\d/g.test(inputs.mdp.value);
    let symboleOK = /\W/g.test(inputs.mdp.value);
    let lettreOK = /[a-zA-Z]/g.test(inputs.mdp.value);
    let tailleOK = inputs.mdp.value.length > 5;

    let nbConditions = (chiffreOK ? 1 : 0)
                        + (symboleOK ? 1 : 0)
                        + (tailleOK ? 1 : 0);
    // updateHint(nbConditions);

    if (chiffreOK && lettreOK && symboleOK && tailleOK) {
        infos.passInfo.style.color = "black";
        passwordOK = true;
    } else {
        infos.passInfo.style.color = "red";
        passwordOK = false;
    }
    // updateBouton();
}