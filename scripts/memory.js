let WIDTH = 2;
let HEIGHT = 3;

onload = init;

let images = [
    "../img/memory-legume/1.svg",
    "../img/memory-legume/1.svg",
    "../img/memory-legume/2.svg",
    "../img/memory-legume/2.svg",
    "../img/memory-legume/3.svg",
    "../img/memory-legume/3.svg",
    "../img/memory-legume/4.svg",
    "../img/memory-legume/4.svg",
    "../img/memory-legume/5.svg",
    "../img/memory-legume/5.svg",
    "../img/memory-legume/6.svg",
    "../img/memory-legume/6.svg"
];

let stack = [];
let isChecking = false;
let cpt = 0;
let victory;
let plateau;

function init() {
    victory = document.getElementById("victory-prompt");
    plateau = document.getElementById("plateau");

    images = images.slice(0, WIDTH * HEIGHT);
    shuffleArray(images);
    initHiddenCards();

    document.addEventListener("keypress", e => {
        if (e.key === " ") {
            shuffleArray(images);
            initHiddenCards();
        }
    });
}

function initHiddenCards() {
    plateau.innerHTML = "";
    victory.style.opacity = "0%";
    cpt = 0;

    if (WIDTH * HEIGHT == images.length) {
        for (let i = 0; i < HEIGHT ; i++) {
            let row = document.createElement("tr");
            for (let j = 0; j < WIDTH; j++) {
                let cell = document.createElement("td");
                let img = document.createElement("img");

                img.classList.add("memory-image");
                img.classList.add("playable");

                img.src = "../img/question.svg";
                img.id = i * WIDTH + j;

                img.addEventListener("click", turnCard);

                cell.appendChild(img);
                row.appendChild(cell);
            }
            plateau.appendChild(row);
        }
    } else {
        let row = document.createElement("tr");
        let cell = document.createElement("td");
        let img = document.createElement("img");

        img.src = "../img/fucking-up.png";
        cell.appendChild(img);
        row.appendChild(cell);
        plateau.appendChild(row);
    }
}

function turnCard(event) {
    if (stack.length == 2)
        return;

    let id = event.srcElement.id;
    event.srcElement.classList.add("rotated");
    event.srcElement.removeEventListener("click", turnCard);
    event.srcElement.classList.remove("playable");

    stack.push(event.srcElement);

    setTimeout(() => {
        event.srcElement.classList.remove("rotated");
        event.srcElement.src = images[id];

        checkCards();
    }, 300);
}

function checkCards() {
    if (stack.length != 2 || isChecking)
        return;

    isChecking = true;
    
    if (images[stack[0].id] == images[stack[1].id]) {
        setTimeout(() => {
            stack[0].style.background = "rgb(202,255,168)";
            stack[1].style.background = "rgb(202,255,168)";

            stack = [];
            isChecking = false;
            cpt += 2;

            checkVictory();
        }, 600);
    } else {
        setTimeout(() => {
            stack[0].classList.add("rotated");
            stack[1].classList.add("rotated");

            setTimeout(() => {
                stack[0].classList.remove("rotated");
                stack[1].classList.remove("rotated");
                stack[0].src = "../img/question.svg";
                stack[1].src = "../img/question.svg";
                stack[0].addEventListener("click", turnCard);
                stack[1].addEventListener("click", turnCard);
                stack[0].classList.add("playable");
                stack[1].classList.add("playable");

                stack = [];
                isChecking = false;
            }, 300);
        }, 2000);    
    }
}

function checkVictory() {
    if (cpt == WIDTH * HEIGHT) {
        victory.style.opacity = "100%";
    }
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}